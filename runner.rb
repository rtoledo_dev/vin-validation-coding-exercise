vin = ARGV[0]
require_relative 'lib/check_vin'
check_vin = CheckVIN.new(vin)
check_vin.get_vin_data
puts "\n\nYou gave me VIN: \t#{vin}\n\n"
puts "Make:\t\t\t#{check_vin.make}"
puts "Manufacturer:\t\t#{check_vin.manufacturer}"
puts "Model:\t\t\t#{check_vin.model}"
puts "Model year:\t\t#{check_vin.model_year}"
