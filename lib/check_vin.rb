class CheckVIN
  attr_accessor :vin, :make, :manufacturer, :model, :model_year, :errors

  def initialize(vin = '')
    @vin = vin
    @errors = []
  end

  def valid_vin?
    map     = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 'X']
    weights = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
    sum = 0
    @vin.split('').each_with_index do |char, i|
      sum += transliterate(char) * weights[i]
    end
    map[sum % 11]
  rescue StandardError => e
    @errors << e.message
    false
  end

  def get_vin_data
    if valid_vin?
      require 'certified'
      require 'net/http'
      require 'json'

      begin
        url = 'https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVINValuesBatch/'
        uri = URI(url)
        response = Net::HTTP.post_form(uri, { 'format' => 'json', 'data' => @vin })
        response_parsed = JSON.parse(response.body, symbolize_names: true)
        if response_parsed[:Count] >= 1
          vin_data = response_parsed[:Results].first
          @make         = vin_data[:Make]
          @manufacturer = vin_data[:Manufacturer]
          @model        = vin_data[:Model]
          @model_year   = vin_data[:ModelYear]
          true
        else
          @errors << 'Empty result for this process'
          false
        end
      rescue StandardError => e
        @errors << e.message
        false
      end
    else
      @errors << "Inform a valid VIN number (invalid: #{@vin})"
      false
    end
  end

  protected

  def transliterate(char)
    '0123456789.ABCDEFGH..JKLMN.P.R..STUVWXYZ'.split('').index(char) % 10
  end
end
