# Overview for Vin Validation Codes

This repository contains working code which demonstrate Ruby integration with VIN Decoder numbers. It's a simple test to demonstrate solutions and example of codes to solve this.

The VIN or vehicle identification number is an (almost) unique identifier used in our Industry to identify assets. More information about VINs can be found here: https://en.wikipedia.org/wiki/Vehicle_identification_number An example of a VIN we see today is:
1XPTD40X6JD456115

Clone this repository:
```
  git clone https://rtoledo_dev@bitbucket.org/rtoledo_dev/vin-validation-coding-exercise.git
```

### Dependencies for development

- rbenv or another ruby version manager
- ruby gems
- bundler

### Using the Vin Validation Codes

First install ruby and dependencies for this project

```
rbenv install 2.7.2
gem install bundler --no-doc
bundle
```

Now with just run in terminal the command with a VIN code

```
bundle exec ruby runner.rb 1XPTD40X6JD45611
```

### Testing

All the tests must use rspec
Use guard for development and test at same time

```
bundle exec guard
```

The result of testing coverage will be in **coverage/index.html**
