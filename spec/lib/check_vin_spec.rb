require 'spec_helper'
require File.dirname(__FILE__) + '/../../lib/check_vin'

RSpec.describe CheckVIN do
  let(:valid_vin) { '1XPTD40X6JD456115' }
  let(:invalid_vin) { 'asdfghj' }

  describe '#initialize' do
    context 'without vin number' do
      subject { CheckVIN.new }
      it 'vin number should be nil' do
        expect(subject.vin).to be_empty
      end
    end

    context 'with vin number' do
      subject { CheckVIN.new(valid_vin) }
      it 'set vin number' do
        expect(subject.vin).to eql(valid_vin)
      end
    end

    context 'empty errors' do
      it 'set empty errors' do
        expect(subject.errors).to be_empty
      end
    end
  end

  describe '#valid_vin?' do
    context 'with valid VIN number' do
      subject { CheckVIN.new(valid_vin) }
      it 'returns positive' do
        expect(subject.valid_vin?).to be_truthy
      end
    end

    context 'with invalid VIN number' do
      subject { CheckVIN.new(invalid_vin) }
      it 'returns negative' do
        expect(subject.valid_vin?).to be_falsy
      end
    end
  end

  describe '#errors' do
    subject { CheckVIN.new(invalid_vin) }

    context 'with invalid vin validation' do
      before do
        subject.valid_vin?
      end

      it 'have one exception message' do
        expect(subject.errors.size).to eql(1)
      end

      it 'have exception message' do
        expect(subject.errors.include?("undefined method `%' for nil:NilClass")).to be_truthy
      end

      context 'with invalid vin number' do
        before do
          subject.stub(:valid_vin?) { false }
        end

        it 'have one error message' do
          expect(subject.errors.size).to eql(1)
        end

        # TODO: Stubs should be are working
        xit 'have error message' do
          expect(subject.errors.include?("Inform a valid VIN number (invalid: #{invalid_vin})")).to be_truthy
        end
      end
    end

    context 'when try get VIN data' do
      before do
        subject.get_vin_data
      end

      it 'have multiple error messages' do
        expect(subject.errors.size).to be >= 2
      end

      it 'have exception message' do
        expect(subject.errors.include?("undefined method `%' for nil:NilClass")).to be_truthy
        expect(subject.errors.include?("Inform a valid VIN number (invalid: #{invalid_vin})")).to be_truthy
      end
    end
  end
end
